﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Application.Dto;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services
{
    public interface IPartnerService
    {
        Task<List<PartnerResponseDto>> GetPartnersAsync();
        
        Task<PartnerResponseDto> GetPartnerByIdAsync(Guid id);

        Task<PartnerPromoCodeLimitResponseDto> GetPartnerLimitAsync(Guid id, Guid limitId);

        Task<Guid> SetPartnerPromoCodeLimitAsync(Guid id, SetPartnerPromoCodeLimitRequestDto requestDto);

        Task CancelPartnerPromoCodeLimitAsync(Guid id);
    }
}