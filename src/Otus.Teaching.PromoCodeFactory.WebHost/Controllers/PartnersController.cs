﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
 using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Gateways;
 using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
 using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
 using Otus.Teaching.PromoCodeFactory.Core.Application.Dto;
 using Otus.Teaching.PromoCodeFactory.Core.Application.Exceptions;
 using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Партнеры
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PartnersController
        : ControllerBase
    {
        private readonly IPartnerService _partnerService;

        public PartnersController(IPartnerService partnerService)
        {
            _partnerService = partnerService;
        }
        
        [HttpGet]
        public async Task<ActionResult<List<PartnerResponseDto>>> GetPartnersAsync()
        {
            var response = await _partnerService.GetPartnersAsync();

            return Ok(response);
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<List<PartnerResponseDto>>> GetPartnersAsync(Guid id)
        {
            try
            {
                var response = await _partnerService.GetPartnerByIdAsync(id);
                return Ok(response);
            }
            catch (EntityNotFoundException)
            {
                return BadRequest();
            }
        }
        
        [HttpGet("{id}/limits/{limitId}")]
        public async Task<ActionResult<PartnerPromoCodeLimitResponseDto>> GetPartnerLimitAsync(Guid id, Guid limitId)
        {
            try
            {
                var response = await _partnerService.GetPartnerLimitAsync(id, limitId);
                return Ok(response);
            }
            catch (EntityNotFoundException)
            {
                return BadRequest();
            }
        }
        
        [HttpPost("{id}/limits")]
        public async Task<IActionResult> SetPartnerPromoCodeLimitAsync(Guid id, SetPartnerPromoCodeLimitRequestDto requestDto)
        {
            try
            {
                var newLimitId = await _partnerService.SetPartnerPromoCodeLimitAsync(id, requestDto);
                return CreatedAtAction(nameof(GetPartnerLimitAsync), new {id = id, limitId = newLimitId.ToString()}, null);
            }
            catch (EntityNotFoundException)
            {
                return NotFound();
            }
            catch(ChangePartnerLimitException)
            {
                return BadRequest();
            }
        }
        
        [HttpPost("{id}/canceledLimits")]
        public async Task<IActionResult> CancelPartnerPromoCodeLimitAsync(Guid id)
        {
            await _partnerService.CancelPartnerPromoCodeLimitAsync(id);
            
            return NoContent();
        }
    }
}