﻿﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public class PromoCodeMapper
    {
        public static PromoCode MapGromModel(GivePromoCodeRequest request, Preference preference, IEnumerable<Customer> customers) {

            var promocode = new PromoCode();
            promocode.Id = Guid.NewGuid();
            
            promocode.PartnerName = request.PartnerName;
            promocode.Code = request.PromoCode;
            promocode.ServiceInfo = request.ServiceInfo;
           
            promocode.BeginDate = DateTime.Now;
            promocode.EndDate = DateTime.Now.AddDays(30);

            promocode.Preference = preference;
            promocode.PreferenceId = preference.Id;

            //promocode.CustomerPromoCodes = new List<CustomerPromoCode>();
            promocode.CustomerPromoCodes = customers.Select(x => new CustomerPromoCode()
            {
                CustomerId = x.Id,
                Customer = x
            }).ToList();


            return promocode;
        }
    }
}
