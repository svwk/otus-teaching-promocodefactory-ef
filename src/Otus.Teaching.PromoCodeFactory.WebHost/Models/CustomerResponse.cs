﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CustomerResponse
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<PreferenceResponse> Preferences { get; set; }
        public List<PromoCodeShortResponse> PromoCodes { get; set; }

        public CustomerResponse()
        {
            
        }

        public CustomerResponse(Customer customer)
        {
            Id = customer.Id;
            Email = customer.Email;
            FirstName = customer.FirstName;
            LastName = customer.LastName;

            //Preferences = new List<PreferenceResponse>();
            Preferences= customer.CustomerPreferences.Select(x => new PreferenceResponse()
            {
                Id = x.Preference.Id,
                Name = x.Preference.Name
            }).ToList();

            
            PromoCodes = new List<PromoCodeShortResponse>();
            PromoCodes = customer.CustomerPromoCodes.Select(x => new PromoCodeShortResponse()
            {
                Id = x.PromoCode.Id,
                BeginDate = x.PromoCode.BeginDate.ToString("yyyy-MM-dd"),
                EndDate = x.PromoCode.EndDate.ToString("yyyy-MM-dd"),
                Code = x.PromoCode.Code,
                ServiceInfo = x.PromoCode.ServiceInfo,
                PartnerName = x.PromoCode.PartnerName

            }).ToList();

        }
    }
}